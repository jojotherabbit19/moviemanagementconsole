﻿using MovieManagement.BussinessLayers;
using MovieManagement.Entities;

namespace MovieManagement
{
    public static class Menu
    {
        public static int MainMenu()
        {
            Console.WriteLine("=========================");
            Console.WriteLine("1. Movie Controller.\n2. Review Controller.\n3. Exit.");
            Console.WriteLine("=========================");
            Console.Write("Enter you choice: ");
            int choice;
            if (int.TryParse(Console.ReadLine(), out choice))
            {
                return choice;
            }

            return 0;
        }
        public static int MovieMenu()
        {
            Console.WriteLine("========================");
            Console.WriteLine("1. Add New Movie.\n" +
                "2. Update a Movie.\n" +
                "3. Delete a Movie.\n" +
                "4. Get all Movies.\n" +
                "5. Get a Movie Reviews.\n" +
                "6. Exit.");
            Console.WriteLine("========================");
            Console.Write("Enter your choice: ");
            int choice;
            if (int.TryParse(Console.ReadLine(), out choice))
            {
                return choice;
            }

            return 0;
        }

        public static Movie AddNewMovie()
        {
            Console.WriteLine("=====================");
            Console.Write("Enter Title: ");
            var title = Console.ReadLine();
            Console.Write("Enter Release Date: ");
            var date = Convert.ToDateTime(Console.ReadLine());
            Console.Write("Enter Director: ");
            var director = Console.ReadLine();
            Console.Write("Enter Description: ");
            var des = Console.ReadLine();
            Console.Write("Enter ImageUrl: ");
            var img = Console.ReadLine();
            Console.Write("Enter Rating: ");
            var rating = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("=====================");
            var movie = new Movie()
            {
                Title = title,
                ReleaseDate = date,
                Director = director,
                Description = des,
                ImageUrl = img,
                Rating = rating
            };

            return movie;
        }

        public static async Task<Movie> UpdateMovieAsync(IUnitOfWork unitOfWork)
        {
            Console.WriteLine("=====================");
            Console.Write("Enter movie Id: ");
            var updateId = Convert.ToInt32(Console.ReadLine());
            var movieObj = await unitOfWork.MovieRepository.GetByIdAsync(updateId);
            if (movieObj != null)
            {
                Console.Write("Enter Title: ");
                var updateTitle = Console.ReadLine();
                Console.Write("Enter Release Date: ");
                var updateDate = Convert.ToDateTime(Console.ReadLine());
                Console.Write("Enter Director: ");
                var updateDirector = Console.ReadLine();
                Console.Write("Enter Description: ");
                var updateDes = Console.ReadLine();
                Console.Write("Enter ImageUrl: ");
                var updateImg = Console.ReadLine();
                Console.Write("Enter Rating: ");
                var updateRating = Convert.ToDecimal(Console.ReadLine());
                Console.WriteLine("=====================");

                // assign update value
                movieObj.Title = string.IsNullOrEmpty(updateTitle) ? movieObj.Title : updateTitle;
                movieObj.ReleaseDate = updateDate;
                movieObj.Director = string.IsNullOrEmpty(updateDirector) ? movieObj.Director : updateDirector;
                movieObj.Description = string.IsNullOrEmpty(updateDes) ? movieObj.Description : updateDes;
                movieObj.ImageUrl = string.IsNullOrEmpty(updateImg) ? movieObj.ImageUrl : updateImg;
                movieObj.Rating = updateRating;

                return movieObj;
            }

            return null;
        }

        public static int ReviewMenu()
        {
            Console.WriteLine("========================");
            Console.WriteLine("1. Add New Review.\n" +
                "2. Update a Review.\n" +
                "3. Delete a Review.\n" +
                "4. Get all Review.\n" +
                "5. Exit.");
            Console.WriteLine("========================");
            Console.Write("Enter your choice: ");
            int choice;
            if (int.TryParse(Console.ReadLine(), out choice))
            {
                return choice;
            }

            return 0;
        }

        public static Review AddNewReview()
        {
            Console.WriteLine("=====================");
            Console.Write("Enter Movie Id: ");
            var movieId = Convert.ToInt32(Console.ReadLine());
            Console.Write("Enter Title: ");
            var title = Console.ReadLine();
            Console.Write("Enter Content: ");
            var content = Console.ReadLine();
            Console.Write("Enter Rating: ");
            var rate = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("=====================");

            var review = new Review()
            {
                Title = title,
                Content = content,
                Rating = rate,
                MovieId = movieId
            };

            return review;
        }

        public static async Task<Review> UpdateReviewAsync(IUnitOfWork unitOfWork)
        {
            Console.WriteLine("=====================");
            Console.Write("Enter Review Id: ");
            var reviewId = Convert.ToInt32(Console.ReadLine());
            var reviewObj = await unitOfWork.ReviewRepository.GetByIdAsync(reviewId);
            if (reviewObj != null)
            {
                Console.Write("Enter Title: ");
                var updateTitle = Console.ReadLine();
                Console.Write("Enter Content: ");
                var updateContent = Console.ReadLine();
                Console.Write("Enter Rating: ");
                var updateRate = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("=====================");

                //assign update value
                reviewObj.Title = string.IsNullOrEmpty(updateTitle) ? reviewObj.Title : updateTitle;
                reviewObj.Content = string.IsNullOrEmpty(updateContent) ? reviewObj.Content : updateContent;
                reviewObj.Rating = updateRate;

                return reviewObj;
            }

            return null;
        }
    }
}
