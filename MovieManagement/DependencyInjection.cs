﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MovieManagement.IRepository;
using MovieManagement.Repository;

namespace MovieManagement
{
    public static class DependencyInjection
    {
        public static IServiceCollection MyServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();

            services.AddDbContext<AppDbContext>(op =>
                                    op.UseSqlServer("Data Source=(local);Initial Catalog=MovieDB;User ID=sa;Password=123;TrustServerCertificate=True")
                                    );

            return services;
        }
    }
}
