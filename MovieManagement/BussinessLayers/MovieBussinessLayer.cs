﻿using MovieManagement.Entities;

namespace MovieManagement.BussinessLayers
{
    public class MovieBussinessLayer
    {
        private readonly IUnitOfWork _unitOfWork;
        public MovieBussinessLayer(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<bool> IdExistAsync(int id) => await _unitOfWork.MovieRepository.ExistId(id);

        public async Task<Movie?> GetMovieById(int id) => await _unitOfWork.MovieRepository.GetByIdAsync(id);

        public async Task AddNewMovie(Movie movie)
        {
            await _unitOfWork.MovieRepository.AddAsync(movie);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
            Console.WriteLine(isSuccess);
        }

        public async Task UpdateMovie(Movie movie)
        {
            _unitOfWork.MovieRepository.Update(movie);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
            Console.WriteLine(isSuccess);
        }

        public async Task DeleteMovie(int id)
        {
            var obj = await _unitOfWork.MovieRepository.GetByIdAsync(id);
            if (obj == null)
            {
                Console.WriteLine($"Not found movie ID: {id}.");
                return;
            }
            _unitOfWork.MovieRepository.SoftDelete(obj);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
            Console.WriteLine(isSuccess);
        }

        public async Task GetAllMovieAsync()
        {
            var myMovieList = await _unitOfWork.MovieRepository.GetAllAsync();
            if (myMovieList != null)
            {
                foreach (var item in myMovieList)
                {
                    Console.WriteLine($"|Title: {item.Title}; " +
                        $"ReleaseDate: {item.ReleaseDate}; " +
                        $"Director: {item.Director}; " +
                        $"Description: {item.Description}; " +
                        $"ImageUrl: {item.ImageUrl}; " +
                        $"Rating: {item.Rating}");
                }
            }
            else
            {
                Console.WriteLine("Movie list is empty.");
            }
        }

        public async Task GetMovieByIdAndReviewsAsync(int id)
        {
            var movie = await _unitOfWork.MovieRepository.GetMovieReviewAsync(id);
            if (movie != null)
            {

                Console.WriteLine($"|Title: {movie.Title}; " +
                        $"ReleaseDate: {movie.ReleaseDate}; " +
                        $"Director: {movie.Director}; " +
                        $"Description: {movie.Description}; " +
                        $"ImageUrl: {movie.ImageUrl}; " +
                        $"Rating: {movie.Rating}");
                var reviewList = movie.Reviews?.ToList();
                if (reviewList != null)
                {
                    Console.WriteLine("Reviews: ");
                    foreach (var item in reviewList)
                    {
                        Console.WriteLine($"|Title: {item.Title}; " +
                            $"Content: {item.Content}; " +
                            $"Rating: {item.Rating}");
                    }
                }
                else
                {
                    Console.WriteLine("This movie has no reviews yet.");
                }
            }
        }
    }
}
