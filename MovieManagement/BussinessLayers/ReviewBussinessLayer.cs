﻿using MovieManagement.Entities;

namespace MovieManagement.BussinessLayers
{
    public class ReviewBussinessLayer
    {
        private readonly IUnitOfWork _unitOfWork;
        public ReviewBussinessLayer(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddNewReview(Review review)
        {
            await _unitOfWork.ReviewRepository.AddAsync(review);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
            Console.WriteLine(isSuccess);
        }
        public async Task UpdateReview(Review review)
        {
            _unitOfWork.ReviewRepository.Update(review);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
            Console.WriteLine(isSuccess);
        }
        public async Task DeleteReview(int id)
        {
            var review = await _unitOfWork.ReviewRepository.GetByIdAsync(id);
            if (review != null)
            {
                _unitOfWork.ReviewRepository.SoftDelete(review);
                var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Saved" : "Failed";
                Console.WriteLine(isSuccess);
            }
            else
            {
                Console.WriteLine($"Not found review has ID {id}");
            }
        }
        public async Task GetReviews()
        {
            var listReview = await _unitOfWork.ReviewRepository.GetReviewsAsync();
            foreach (var review in listReview)
            {
                Console.WriteLine($"|Movie: {review.Movie.Title}; " +
                    $"Tilte: {review.Title}; " +
                    $"Content: {review.Content}; " +
                    $"Rating: {review.Rating}");
            }
        }
        public async Task<Review?> GetReviewById(int id) => await _unitOfWork.ReviewRepository.GetByIdAsync(id);
    }
}
