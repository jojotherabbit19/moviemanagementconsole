﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Entities;
using MovieManagement.IRepository;

namespace MovieManagement.Repository
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<List<Review>> GetReviewsAsync() => await _dbSet.Include(x => x.Movie).ToListAsync();
    }
}
