﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Entities;
using MovieManagement.IRepository;

namespace MovieManagement.Repository
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<Movie?> GetMovieReviewAsync(int? id) => await _dbSet.Include(x => x.Reviews).FirstOrDefaultAsync(x => x.Id == id);
    }
}
