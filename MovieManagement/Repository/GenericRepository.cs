﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Entities;
using MovieManagement.IRepository;

namespace MovieManagement.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DbSet<T> _dbSet;
        private readonly AppDbContext _appDbContext;
        public GenericRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
            _dbSet = _appDbContext.Set<T>();
        }
        public async Task AddAsync(T entity)
        {
            await _dbSet.AddAsync(entity);
        }

        public async Task<List<T>?> GetAllAsync(int pageIndex = 0, int pageSize = 10)
        {
            var result = await _dbSet.Skip(pageIndex * pageSize)
                                     .Take(pageSize)
                                     .AsNoTracking()
                                     .ToListAsync();
            return result;
        }

        public async Task<T?> GetByIdAsync(int? id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public void SoftDelete(T entity)
        {
            entity.IsDeleted = true;
        }

        public void Update(T entity)
        {
            _dbSet.Update(entity);
        }

        public Task<bool> ExistId(int id) => _dbSet.AnyAsync(x => x.Id == id);
    }
}
