﻿using MovieManagement.IRepository;
using MovieManagement.Repository;

namespace MovieManagement
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;
        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IMovieRepository MovieRepository => new MovieRepository(_appDbContext);

        public IReviewRepository ReviewRepository => new ReviewRepository(_appDbContext);

        public Task<int> SaveChangeAsync() => _appDbContext.SaveChangesAsync();
    }
}
