﻿using MovieManagement.IRepository;

namespace MovieManagement
{
    public interface IUnitOfWork
    {
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        Task<int> SaveChangeAsync();
    }
}
