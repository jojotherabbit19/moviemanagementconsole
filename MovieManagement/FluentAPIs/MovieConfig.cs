﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieManagement.Entities;

namespace MovieManagement.FluentAPIs
{
    public class MovieConfig : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasIndex(x => x.Title).IsUnique();

            builder.HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
