﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieManagement.Entities;

namespace MovieManagement.FluentAPIs
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasOne(x => x.Movie)
                   .WithMany(x => x.Reviews)
                   .HasForeignKey(x => x.MovieId);

            builder.HasQueryFilter(x => !x.IsDeleted);
        }
    }
}
