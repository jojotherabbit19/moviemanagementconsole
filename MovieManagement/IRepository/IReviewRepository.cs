﻿using MovieManagement.Entities;

namespace MovieManagement.IRepository
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
        Task<List<Review>> GetReviewsAsync();
    }
}
