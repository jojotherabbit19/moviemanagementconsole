﻿using MovieManagement.Entities;

namespace MovieManagement.IRepository
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        Task<Movie?> GetMovieReviewAsync(int? id);
    }
}
