﻿using MovieManagement.Entities;

namespace MovieManagement.IRepository
{
    public interface IGenericRepository<T> where T : BaseEntity
    {
        Task AddAsync(T entity);
        void Update(T entity);
        void SoftDelete(T entity);
        Task<List<T>?> GetAllAsync(int pageIndex = 0, int pageSize = 10);
        Task<T?> GetByIdAsync(int? id);
        Task<bool> ExistId(int id);
    }
}
