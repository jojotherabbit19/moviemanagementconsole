﻿using Microsoft.Extensions.DependencyInjection;
using MovieManagement;
using MovieManagement.BussinessLayers;

try
{
    // call Controller
    await ServicesAsync();
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}


// main controller
async Task ServicesAsync()
{
    var service = new ServiceCollection();
    var myServices = DependencyInjection.MyServices(service).BuildServiceProvider();
    var unitOfWorkService = myServices.GetService<IUnitOfWork>()!;
    //
    var movieBussinessLayer = new MovieBussinessLayer(unitOfWorkService);
    var reviewBussinessLayer = new ReviewBussinessLayer(unitOfWorkService);
    //
    bool con = true;

    do
    {
        switch (Menu.MainMenu())
        {
            case 1:
                await MovieServicesAsync(movieBussinessLayer, unitOfWorkService);
                break;
            case 2:
                await ReviewServicesAsync(reviewBussinessLayer, unitOfWorkService);
                break;
            case 3:
                Console.WriteLine("Good bye.");
                con = false;
                break;
            default:
                break;
        }
    } while (con);
}

// movie controller
async Task MovieServicesAsync(MovieBussinessLayer movieBussinessLayer, IUnitOfWork unitOfWork)
{
    var con = true;

    do
    {
        switch (Menu.MovieMenu())
        {
            case 1:
                await movieBussinessLayer.AddNewMovie(Menu.AddNewMovie());
                break;
            case 2:
                await movieBussinessLayer.UpdateMovie(await Menu.UpdateMovieAsync(unitOfWork));
                break;
            case 3:
                Console.WriteLine("Enter movie Id: ");
                int delId;
                if (int.TryParse(Console.ReadLine(), out delId))
                {
                    await movieBussinessLayer.DeleteMovie(delId);
                }
                break;
            case 4:
                await movieBussinessLayer.GetAllMovieAsync();
                break;
            case 5:
                Console.Write("Enter movie Id: ");
                int id;
                if (int.TryParse(Console.ReadLine(), out id))
                {
                    await movieBussinessLayer.GetMovieByIdAndReviewsAsync(id);
                }
                break;
            case 6:
                Console.WriteLine("Good bye!");
                con = false;
                break;
            default:
                break;
        }
    } while (con);
}

async Task ReviewServicesAsync(ReviewBussinessLayer reviewBussinessLayer, IUnitOfWork unitOfWork)
{
    var con = true;
    do
    {
        switch (Menu.ReviewMenu())
        {
            case 1:
                await reviewBussinessLayer.AddNewReview(Menu.AddNewReview());
                break;
            case 2:
                await reviewBussinessLayer.UpdateReview(await Menu.UpdateReviewAsync(unitOfWork));
                break;
            case 3:
                Console.WriteLine("Enter review Id: ");
                int delId;
                if (int.TryParse(Console.ReadLine(), out delId))
                {
                    await reviewBussinessLayer.DeleteReview(delId);
                }
                break;
            case 4:
                await reviewBussinessLayer.GetReviews();
                break;
            case 5:
                Console.WriteLine("Good bye!");
                con = false;
                break;
            default:
                break;
        }
    } while (con);
}